import React from 'react'
import './form.style.css'

const Form =(props)=>{

    return(
       <form onSubmit={props.getWeather}>
            <div className='container'>
            <div> {props.error ? error() :null}</div>
<div className='row'>
<div className='col-md-3 offset-md-2'>
<input type='text' className='form-control' name='city' placeholder='City' autoComplete='off'>

</input>
</div>

<div className='col-md-3'>
<input type='text' className='form-control' name='country'  placeholder='Country' autoComplete='off'>
</input>
</div>

<div className='col-md-3 mt-md-0 text-md-left'>

<button className='btn btn-warning'>Get Weather </button>


</div>

</div>
        </div>
       </form>
    )
function error(){
    return(
        
        <div className='alert alert-warning mx-5' role='alert'>Please Enter City & Country Names</div>
    )
}

}

export default Form;